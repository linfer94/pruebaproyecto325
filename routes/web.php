<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TemaController;
use App\Http\Controllers\AsignacionController;
use App\Http\Controllers\UsuarioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/temas', [TemaController::class, 'index'])->name('temas');
Route::get('/asignar', [AsignacionController::class, 'index'])->name('asignar');
Route::get('/usuario', [UsuarioController::class, 'index'])->name('usuario');

Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');
