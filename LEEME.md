## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## PASOS DE CONFIGURACION

## Pasos para correr el proyecto
    composer install
    npm install
    npm run dev
## crear y configurar el archivo .env copiando el archivo .env.example
    colocar el mismo nombre de la base de datos 'bancotemas' al crear y agregar al archivo .env

## ejecutar las migraciones y seeder
    php artisan migrate
    php artisan db:seed --class=UserSeeder

##    Autenticacion por defecto
    user: adminusfx@gmail.com       password: adminusfx

##
