<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'name' => 'adminusfx',
            'email' => 'adminusfx@gmail.com',
            'password' => bcrypt('adminusfx'), // Reemplaza 'password' con la contraseña deseada
        ]);
    }
}
